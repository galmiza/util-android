## Introduction

Let's admit it, Java can be a huge pain when it comes to perform very basic operations, like reading a file, sending a HTTP request...  

Hence this library that simplifies such operations. It is organised into classes:  

  - Asset (to read assets stored within the application)
  - File (easy read/write/append/copy/move files, get files list, zip/unzip)
  - Image (crop image, download from url)
  - Index (fast string completion suggestions)
  - Log (log with automatic device info and share over internet)
  - Media (manage the internal device audio media library)
  - Misc (various android specific functions)
  - Network (get/post data over http)
  - Security (md5 and aes/cbc encryption)

## Prototypes

Function names and their prototypes well describes the functions. Additional information are provided when needed.  
All functions are static and can be invoked like:
```
#!java
<ClassName>.<FunctionName>(...) { // ...
Asset.getAssetList(...) { // ...
```

#### Asset
```
#!java
List<String> getAssetList(Activity a, String path, boolean absolute) throws Exception;
void getAssetListRecursive(Activity a, List<String> list, String path) throws Exception;
void moveAssets(Activity a, String path, File dst) throws Exception;
```

#### File
```
#!java
void write(String path, String data, boolean append) throws Exception;
void write(java.io.File file, String data, boolean append) throws Exception;
String read(java.io.File file) throws Exception;
String read(String path) throws Exception;
byte[] readBinary(java.io.File file) throws Exception;
void writeBinary(java.io.File file, byte[] bytes, boolean append) throws Exception;
void copy(java.io.File src, java.io.File dst) throws Exception;
void copyStream(InputStream in, OutputStream out) throws Exception;
void deleteRecursive(java.io.File dir);
void copyRecursive(java.io.File src, java.io.File dst) throws Exception;
List<java.io.File> getFilelist(java.io.File path, String extension);
void getFilelistRecursive(List<java.io.File> filelist, java.io.File path, String extension);
int getPathSize(java.io.File path);
void zip(java.io.File src, java.io.File dst) throws Exception;
void unzip(java.io.File src, java.io.File dst) throws Exception;
```

#### Image
```
#!java
Bitmap bitmapCropSquare(Bitmap in);
int getDownsampleScale(int width, int height, int max);
Bitmap getDownsampledBitmap(Activity a, Uri uri, int max) throws Exception;
```

#### Index
```
#!java
void addSuggestion(String s, Object o) throws Exception;
void removeSuggestion(Object o) throws Exception;
Set<Object> getSuggestions(String query) throws Exception;
```

#### Log
```
#!java
void enable();
void disable();
void d(String tag, String text);
void init(Activity a, String url, String key);
void add(String s);
void add(Exception e);
void send(Activity a);
```

#### Media
```
#!java
List<Object> getAudioMediaList(Activity a);
String getMediaPathFromTrackDetails(Activity a, String artist, String title, String album);
Object getMediaFromPath(Activity a, String path);
String getRealPathFromURI(Activity a, Uri contentUri);
```

#### Misc
```
#!java
Object getAttribute(String s);
void setAttribute(String s, Object o);
void resetAttributes();
void sleep(int ms);
void toast(final Activity a, final String message, final int duration);
void toast(final String message, final int duration);
String unicodeEscape(String s);
boolean isIntentAvailable(Context context, String action);
void getOverflowMenu(Context context);
void onSimpleInputDialog(final Context context, String title, String message, String text, final OnSimpleInputDialogListener listener);
static void onMultiInputHintDialog(final Context context, String title, String message, String[] hints, String[] texts, final OnMultiInputHintDialogListener listener);
ProgressDialog progressDialog(Context context, String title, String message, boolean cancelable, boolean canceledOnTouchOutside);
```

#### Network
```
#!java
byte[] httpGetBinary(String url) throws Exception;
String httpGetString(String url, String charset) throws Exception;
byte[] httpPost(String url, byte[] bytes) throws Exception;
String httpPost(String url, List<NameValuePair> args) throws Exception;
void httpPostAsync(final String url, final byte[] b, final OnFinishListener listener);
void httpGetAsync(final String url, final OnFinishListener listener);
void downloadToFile(URL url, File file) throws Exception;
```

#### Security
```
#!java
byte[] md5(byte[] in) throws Exception;
String encodeHexString(byte[] bytes);
byte[] encrypt(byte[] data, byte[] key) throws Exception;
byte[] decrypt(byte[] data, byte[] key) throws Exception;
String encrypt(String data, String key) throws Exception;
String decrypt(String data, String key) throws Exception;
```