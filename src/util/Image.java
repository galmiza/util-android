package util;

import java.io.InputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

public class Image {
    /**
     * BITMAP
     */
    public static Bitmap bitmapCropSquare(Bitmap in) {
    	Bitmap out;
    	if (in.getWidth() >= in.getHeight()) {
    		out = Bitmap.createBitmap(
    				in, 
    				in.getWidth()/2 - in.getHeight()/2,
    				0,
    				in.getHeight(),
    				in.getHeight());
    	} else {
    		out = Bitmap.createBitmap(
    				in,
    				0, 
    				in.getHeight()/2 - in.getWidth()/2,
    				in.getWidth(),
    				in.getWidth());
    	}
    	return out;
    }
    
    public static int getDownsampleScale(int width, int height, int max) {
    	int scale = 1;
    	while (width > max && height > max) {
    		width /= 2;
    		height /= 2;
            scale *= 2;
    	}
    	return scale;
    }
    
    public static Bitmap getDownsampledBitmap(Activity a, Uri uri, int max) throws Exception {
    	
    	// Get dimensions
		InputStream is = a.getContentResolver().openInputStream(uri);
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(is, null, o);
		int width = o.outWidth;
		int height = o.outHeight;
		
		// Get down sampled bitmap
		is = a.getContentResolver().openInputStream(uri);
		o = new BitmapFactory.Options();
		o.inJustDecodeBounds = false;
		o.inSampleSize = getDownsampleScale(width, height, max);
		return BitmapFactory.decodeStream(is, null, o);
    }
    
}
