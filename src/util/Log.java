package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageInfo;

/**
 * Can be disabled so code doesn't have to be removed
 * Proguard could remove it too (Warning, Ads do not work if disabling android.util.Log!)
 * 
 * Can also buffer data into file
 */
public class Log {
	private static boolean enabled = false; // Enable/Disable logcat print
	private static java.io.File file;
	private static String FILENAME_LOG = "log.txt";
	private static String URL_LOG_REPORTING;
	private static String ENCRYPTION_LOG_KEY;

	public static void enable() { enabled = true; }
	public static void disable() { enabled = false; }
	
	// Simple log as android.util.Log.d
	public static void d(String tag, String text) {
		if (enabled) {
			android.util.Log.d(tag, text);
		}
	}
	
	// Set file
	public static void init(Activity a, String url, String key) {
		URL_LOG_REPORTING = url;
		ENCRYPTION_LOG_KEY = key;
		file = new java.io.File(a.getFilesDir(), FILENAME_LOG);
	}
	
	/**
	 * Add a line to a file
	 * Date is automatically added
	 */
	public static void add(String s) {
		try {
			File.write(file, getDateString()+"\t"+s+"\n", true);
		} catch (Exception e) { e.printStackTrace(); } // No critical
	}
	public static void add(Exception e) {
		try {
			File.write(file, getDateString()+"\t"+Error.getStackString(e)+"\n", true);
		} catch (Exception e_) { e_.printStackTrace(); } // No critical
	}
	
	/**
	 * Return a string representing the date with the format
	 * 2013-09-14 13:09:21 (Europe/Paris)
	 */
	@SuppressLint("DefaultLocale")
	public static String getDateString() {
		TimeZone tz = Calendar.getInstance().getTimeZone();    
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale.US);
		return String.format("%1$s (%2$s)", sdf.format(new Date()), tz.getID());
	}
	
	/**
	 * Send the log to server and purge file
	 * In case of error during log processing, file will not be deleted
	 */
	@SuppressLint("DefaultLocale")
	public static void send(Activity a) {
		try {
			// Send only if something has to be send
			if (file.length()>0) {
				
				// Retrieve contextual informations
				String packageName = a.getPackageName();
				PackageInfo pInfo = a.getPackageManager().getPackageInfo(packageName, 0);
				String header = String.format("%1$s %2$s (%3$d)\n\n",  packageName, pInfo.versionName, pInfo.versionCode);
				
				// Encrypt content
				String charset = "US-ASCII";
				byte[] data = Security.encrypt(
						(header+File.read(file)).getBytes(charset),
						Security.md5(ENCRYPTION_LOG_KEY.getBytes(charset)));
				
				// Send to server
				// And delete files on success
				Network.httpPostAsync(
						URL_LOG_REPORTING,
						data,
						new Network.OnFinishListener() {
							@Override
							public void onFinish(byte[] result) {
								file.delete();
							}
						});
			}
		} catch (Exception e) {	e.printStackTrace(); }
	}
	
}
