package util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class Network {


	// Raw http get
	public static byte[] httpGetBinary(String url) throws Exception {
		byte[] out = null;
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		HttpResponse responseGet = client.execute(get);  
		HttpEntity resEntityGet = responseGet.getEntity();  
		if (resEntityGet != null)
			out = EntityUtils.toByteArray(resEntityGet);
		return out;
	}
	public static String httpGetString(String url, String charset) throws Exception {

		String out = null;
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		HttpResponse responseGet = client.execute(get);  
		HttpEntity resEntityGet = responseGet.getEntity();  
		if (resEntityGet != null)
		   out = EntityUtils.toString(resEntityGet, charset);
		return out;
	 }
	
	// Raw http post
	public static byte[] httpPost(String url, byte[] bytes) throws Exception {
		byte[] out = null;
    	HttpClient httpClient = new DefaultHttpClient();
	    HttpPost httpPost = new HttpPost(url);
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		httpPost.setEntity(entity);
	    HttpResponse response = httpClient.execute(httpPost);
	     
	    StatusLine statusLine = response.getStatusLine();
		if(statusLine.getStatusCode() == HttpStatus.SC_OK){
	        ByteArrayOutputStream bs = new ByteArrayOutputStream();
	        response.getEntity().writeTo(bs);
	        bs.close();
	        out = bs.toByteArray();
	    } else {
	        response.getEntity().getContent().close();
	    }
	    return out;
	}
	
	public static String httpPost(String url, List<NameValuePair> args) throws Exception {
		String out = null;
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(new UrlEncodedFormEntity(args));
		HttpResponse response = httpClient.execute(httpPost);
		
		StatusLine statusLine = response.getStatusLine();
		if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
			ByteArrayOutputStream bs = new ByteArrayOutputStream();
			response.getEntity().writeTo(bs);
			bs.close();
			out = bs.toString("UTF-8");
		} else {
			response.getEntity().getContent().close();
		}
		return out;
	}
	
	// Http post raw message
	public static interface OnFinishListener {
        void onFinish(byte[] result);
    }
	public static void httpPostAsync(final String url, final byte[] b, final OnFinishListener listener) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					byte[] r = httpPost(url, b);
					if (listener != null)
						listener.onFinish(r);
				} catch (Exception e) { e.printStackTrace(); }
			}}).start();
	}
	public static void httpGetAsync(final String url, final OnFinishListener listener) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					byte[] r = httpGetBinary(url);
					if (listener != null)
						listener.onFinish(r);
				} catch (Exception e) { e.printStackTrace(); }
			}}).start();
	}	
	
	public static void downloadToFile(URL url, File file) throws Exception {
		// Open a connection to that URL
		URLConnection ucon = url.openConnection();
			
		// Define InputStreams to read from the URLConnection
		InputStream is = ucon.getInputStream();
		BufferedInputStream bis = new BufferedInputStream(is);
			
		// Read bytes to the Buffer until there is nothing more to read(-1)
		FileOutputStream fos = new FileOutputStream(file);
		int current = 0;
		while ((current = bis.read()) != -1) {
			fos.write(current);
			fos.flush();
		}
		fos.close();
	}

}
