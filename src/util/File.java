package util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * 
 * WARNING: charset is assumed to be UTF-8 in most functions
 *
 */
public class File {
	
	public static void write(String path, String data, boolean append) throws Exception {
		if (path == null) return;
		write(new java.io.File(path), data, append);
	}
	
	public static void write(java.io.File file, String data, boolean append) throws Exception {
		writeBinary(file, data.getBytes("UTF-8"), append);
	}
	
	public static String read(java.io.File file) throws Exception {
		return new String(readBinary(file), "UTF-8");
	}
	
	public static String read(String path) throws Exception {
		return read(new java.io.File(path));
	}

	public static byte[] readBinary(java.io.File file) throws Exception {
		int l = (int) file.length();
		byte[] b = new byte[l];
		FileInputStream fis = new FileInputStream(file);
		fis.read(b, 0, l);
		fis.close();
		return b;
	}
	
	public static void writeBinary(java.io.File file, byte[] bytes, boolean append) throws Exception {
		if (file.exists() == false) {
			file.createNewFile();
		}
		if (file.canWrite()) {
			FileOutputStream fos = new FileOutputStream(file, append);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			bos.write(bytes);
			bos.flush();
			bos.close();
		}
	}
	
	public static void copy(java.io.File src, java.io.File dst) throws Exception {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);
		copyStream(in, out);
	}
	
	public static void copyStream(InputStream in, OutputStream out) throws Exception {
		byte[] buffer = new byte[1024];
		int read;
		while((read = in.read(buffer)) != -1)
			out.write(buffer, 0, read);
	}
	
	public static void deleteRecursive(java.io.File dir) {
	    if (dir.isDirectory())
	        for (java.io.File child : dir.listFiles())
	            deleteRecursive(child);
	    dir.delete();
	}

	public static void copyRecursive(java.io.File src, java.io.File dst) throws Exception {
		for (java.io.File f : getFilelist(src, null)) {
			if (f.isFile()) {
				String name = f.getAbsolutePath().replace(src.getAbsolutePath(),"");
				java.io.File dst_ = new java.io.File(dst, name);
				dst_.getParentFile().mkdirs();
				copy(f, dst_);
			}
		}
	}
	
	
    /**
     * Get file list
     */
    public static List<java.io.File> getFilelist(java.io.File path, String extension) {
    	List<java.io.File> filelist = new ArrayList<java.io.File>();
    	getFilelistRecursive(filelist, path, extension);
		return filelist;
    }

	private static void getFilelistRecursive(List<java.io.File> filelist, java.io.File path, String extension) {
		if (path != null && path.isDirectory()) {
			java.io.File[] files = path.listFiles();
			if (files != null)
				for (java.io.File f : files) {
					if (f != null)
						if (extension == null)
							filelist.add(f);
						else
							if (f.getName().endsWith(extension))
								filelist.add(f);
					getFilelistRecursive(filelist, f, extension);
				}
	     }
	}
	
	public static int getPathSize(java.io.File path) {
		int size = 0;
		List<java.io.File> files = getFilelist(path, null);
		for (java.io.File f : files) {
			if (f.isFile()) 			size += f.length();
			else if (f.isDirectory())	size += getPathSize(f);
		}
		return size;
	}
	
	/**
	 * Zip/Unzip files and folders
	 * Local file structure is copied to the destination zip
	 */
	public static void zip(java.io.File src, java.io.File dst) throws Exception {
		
		List<java.io.File> files = getFilelist(src, null);
		int offset = src.getAbsolutePath().length()+1;

		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(dst));
		for (java.io.File f : files) {
			if (f.isFile()) {
				zos.putNextEntry(new ZipEntry(f.getAbsolutePath().substring(offset)));
				zos.write(readBinary(f));
			}
		}
		zos.close();
	}
	
	public static void unzip(java.io.File src, java.io.File dst) throws Exception {
		ZipInputStream zis = new ZipInputStream(new FileInputStream(src));
		ZipEntry ze;
		while ((ze = zis.getNextEntry()) != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int count;
			while ((count = zis.read(buffer)) != -1) {
				baos.write(buffer, 0, count);
			}
			byte[] bytes = baos.toByteArray();
			java.io.File f = new java.io.File(dst, ze.getName());
			f.getParentFile().mkdirs();
			writeBinary(f, bytes, false);
		}
		zis.close();
	}
}
