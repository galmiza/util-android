package util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.res.AssetManager;

public class Asset {
	
	public static List<String> getAssetList(Activity a, String path, boolean absolute) throws Exception {
		List<String> list = new ArrayList<String>();
		getAssetListRecursive(a, list, path);
		if (!absolute && path.length()>0) {
			list.remove(path);
			int offset = path.length()+1;
			int l = list.size();
			for (int i=0; i<l; i++) {
				String s = list.get(i);
				list.set(i,s.substring(offset));
			}
		}
		return list;
	}
	
	private static void getAssetListRecursive(Activity a, List<String> list, String path) throws Exception {
		AssetManager assetManager = a.getAssets();
		String[] assets = assetManager.list(path);
		if (assets.length==0) {
			list.add(path);
    	} else {
    		for (String asset : assets) {
    			getAssetListRecursive(a, list, path.length()==0 ? asset : path+"/"+asset);
            }
    	}
	}
	
	public static void moveAssets(Activity a, String path, File dst) throws Exception {
		AssetManager assetManager = a.getAssets();
		List<String> list = getAssetList(a, path, false);
		for (String asset : list) {
			InputStream in = assetManager.open(path.length()==0 ? asset : path+"/"+asset);
			File file = new File(dst, asset);
			if (file.isDirectory()) net.galmiza.android.util.File.deleteRecursive(file);
			file.getParentFile().mkdirs();
		    OutputStream out = new FileOutputStream(file);
		    net.galmiza.android.util.File.copyStream(in, out); 
		}
	}
}
