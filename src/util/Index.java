package util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import java.util.Locale;

/**
 * Index objects provide suggestions based on user inputs
 * They are based on an associative array (key=>value) where:
 *  The keys represent parts of user inputs
 *  The values represent the suggestions
 * 
 * Example:
 *  To provide suggestions for the sentences "Hello mom!" and "Hell yeah"
 *  The keys "h,he,hel,hell,hello,m,mo,mom" are given to the value "Hello mom!"
 *  The keys "h,he,hel,hell,y,ye,ye" are given to the value "Hell yeah"
 *  Hence, whenever the user input contains a word that matchs a key, the corresponding suggestions are considered
 *  Suggestions that are considered for every words in the user input are returned
 *   Input "hel" will output both suggestions "Hello mom!" and "Hell yeah"
 *   Input "hello" will only output suggestion "Hello mom!"
 *   Input "hel y" will only output suggestion "Hell yeah"
 * 
 * Learning phase (addSuggestion, removeSuggestion):
 *  The index object is given the list of suggestions (typically sentences)
 *  For each suggestion,
 *   It calculates all possible user inputs that would lead to the suggestion (all substrings of each word starting with the first char)
 *   And add the association 'possible input'=>'suggestion' to the array
 * 
 * Execution phase (getSuggestions):
 *  The intersection of the suggestions for the words from the user input is returned
 * 
 */
public class Index {
	
	private HashMap<String,Set<Object>> map = new HashMap<String,Set<Object>>();
	private String patternSplit = "[ \\(\\)\\[\\]\\{\\}'\"_\\-,\\.;&]";
	private int minChar = 1;
	private int maxChar = 20;

	public void addSuggestion(String s, Object o) throws Exception {
		if (o==null) o=s;
		
		// Get words from simplified string
		String words[] = s.toLowerCase(Locale.getDefault()).split(patternSplit);
		for (String w : words) {
			
			// Get char sequences
			int l = Math.min(w.length(),maxChar);
			for (int i=minChar-1; i<l; i++) {
				String c = w.substring(0,i+1);
				Set<Object> set = map.get(c);
				if (set == null) // Create set for the new user input
					set = new HashSet<Object>();
				set.add(o);
				map.put(c, set);
			}
		}
	}

	public void removeSuggestion(Object o) throws Exception {
		for (Set<Object> set : map.values()) {
			if (set.remove(o)) {
				if (set.size()==0) {
					map.remove(set);
				}
			}
		}
	}
	
	// Never returns null
	public Set<Object> getSuggestions(String query) throws Exception {
		
		// Get words from string
		String words[] = query.split(patternSplit);
		Set<Object> output = new HashSet<Object>();
		for (String s : words) {
			if (s.length()<minChar) continue;
			Set<Object> set = map.get(s);
			if (set==null) return null;
			if (output.isEmpty())	output.addAll(map.get(s));
			else					output.retainAll(set);
		}
		return output;
	}
	
	// DEBUG
	String print() {
		return map.toString();
	}
}
