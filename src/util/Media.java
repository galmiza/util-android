package util;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

/**
 * 
 * No exceptions?
 */
public class Media {

	public static class Object {
		public String artist;
		public String title;
		public String album;
		public long album_id;;
		public String year;
		public String track;
		
		Object(String artist, String title, String album, long album_id, String year, String track) {
			this.artist = artist;
			this.title = title;
			this.album = album;
			this.album_id = album_id;
			this.year = year;
			this.track = track;
		}
	}
	
	public static List<Object> getAudioMediaList(Activity a) {
		
		// Some audio may be explicitly marked as not being music
		String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
			
		String[] projection = {
		        MediaStore.Audio.Media._ID,
		        MediaStore.Audio.Media.ARTIST,
		        MediaStore.Audio.Media.TITLE,
		        MediaStore.Audio.Media.ALBUM,
		        MediaStore.Audio.Media.ALBUM_ID,
		        MediaStore.Audio.Media.YEAR,
		        MediaStore.Audio.Media.TRACK
		};
			
		List<Object> medias = new ArrayList<Object>();
		
		Cursor cursor;
		
		// External content
		cursor = a.getContentResolver().query(
		        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
		        projection,
		        selection,
		        null,
		        null);
		while (cursor.moveToNext())
			medias.add(new Object(
					cursor.getString(1),
					cursor.getString(2),
					cursor.getString(3),
					cursor.getInt(4),
					cursor.getString(5),
					cursor.getString(6)));
	
				
		// Internal content
		cursor = a.getContentResolver().query(
		        MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
		        projection,
		        selection,
		        null,
		        null);
		while (cursor.moveToNext())
			medias.add(new Object(
					cursor.getString(1),
					cursor.getString(2),
					cursor.getString(3),
					cursor.getInt(4),
					cursor.getString(5),
					cursor.getString(6)));

		// Return list
		cursor.close();
		return medias;
	}

	public static String getMediaPathFromTrackDetails(Activity a, String artist, String title, String album) {
		
		// Some audio may be explicitly marked as not being music
		String selection =
					MediaStore.Audio.Media.IS_MUSIC + " != 0 and "
				+	MediaStore.Audio.Media.ARTIST + " == \"" + artist.replace("\"", "\"\"") + "\" and "
				+	MediaStore.Audio.Media.TITLE + " == \"" + title.replace("\"", "\"\"") + "\" and "
				+	MediaStore.Audio.Media.ALBUM + " == \"" + album.replace("\"", "\"\"") + "\"";
		String[] projection = { MediaStore.Audio.Media.DATA };

		String path = null;
		
		
		// External content
		if (path == null) {
			Cursor cursor = a.getContentResolver().query(
			        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
			        projection,
			        selection,
			        null,
			        null);
		
			if (cursor.moveToNext())
				path = cursor.getString(0);
			cursor.close();
		}

		
		// Internal content
		if (path == null) {
			Cursor cursor = a.getContentResolver().query(
			        MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
			        projection,
			        selection,
			        null,
			        null);
		
			if (cursor.moveToNext())
				path = cursor.getString(0);
			cursor.close();
		}
		
		// If not found
		return path;
	}
	

	public static Object getMediaFromPath(Activity a, String path) {
		String[] projection = { MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ALBUM_ID };
	    String selection = MediaStore.Audio.Media.DATA + " == ?";
	    String[] selectionArgs = { path };
	    		
	    Cursor cursor = a.getContentResolver().query(
	            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
	            projection,
	            selection,
	            selectionArgs,
	            null);
	    		
	    if (cursor.moveToNext() == false) {
	    	cursor.close();
	    	return null;
	    }
	    Object m = new Object(
	    		cursor.getString(1),
				cursor.getString(2),
				cursor.getString(3),
				cursor.getInt(4),
				cursor.getString(5),
				cursor.getString(6));
	    cursor.close();
	    return m;
	}
	
	public static String getRealPathFromURI(Activity a, Uri contentUri) throws Exception {
        String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = a.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String out = cursor.getString(column_index);
        cursor.close();
        return out;
    }
}
