package util;

import java.security.MessageDigest;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

public class Security {
	
	public static String CHARSET = "UTF-8";
	
	/**
	 * Hash
	 */
	public static byte[] md5(byte[] in) throws Exception {
		MessageDigest digest = MessageDigest.getInstance("MD5");
		digest.reset();
		digest.update(in);
		return digest.digest();
	}
	public static String encodeHexString(byte[] bytes) {
	    StringBuilder result = new StringBuilder();
	    for (byte b : bytes)
	        result.append(Integer.toHexString(b));
	    return result.toString();
	}
	
	/**
	 * AES cipher
	 */
    public static byte[] encrypt(byte[] data, byte[] key) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		Integer iv_size = cipher.getBlockSize();
		byte[] iv_bytes = new byte[iv_size];
	    SecureRandom random = new SecureRandom();
	    random.nextBytes(iv_bytes);
	    SecretKeySpec key_spec = new SecretKeySpec(key,"AES");
	    IvParameterSpec iv_spec = new IvParameterSpec(iv_bytes);
	    cipher.init(Cipher.ENCRYPT_MODE, key_spec, iv_spec);
	    data = cipher.doFinal(data);
	    byte[] enc_bytes = new byte[iv_size+data.length];
	    System.arraycopy(iv_bytes,0,enc_bytes,0,iv_size);
	    System.arraycopy(data,0,enc_bytes,iv_size,data.length);
	    data = enc_bytes;
	    data = Base64.encode(data,Base64.NO_WRAP);
    	return data;
    }
    
    public static byte[] decrypt(byte[] data, byte[] key) throws Exception  {
		data = Base64.decode(data,Base64.NO_WRAP);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		Integer iv_size = cipher.getBlockSize();
		byte[] iv_bytes = new byte[iv_size];
		System.arraycopy(data,0,iv_bytes,0,iv_size);
		byte[] enc_bytes = new byte[data.length-iv_size];
		System.arraycopy(data,iv_size,enc_bytes,0,data.length-iv_size);
		data = enc_bytes;
	    SecretKeySpec key_spec = new SecretKeySpec(key,"AES");
	    IvParameterSpec iv_spec = new IvParameterSpec(iv_bytes);
	    cipher.init(Cipher.DECRYPT_MODE, key_spec, iv_spec);
	    data = cipher.doFinal(data);
    	return data;
    }
  
    public static String encrypt(String data, String key) throws Exception {
    	String out = null;
		byte[] k = key.getBytes(CHARSET);
		byte[] d = data.getBytes(CHARSET);
		out = new String(encrypt(d,k), CHARSET);
    	return out;
    }
    public static String decrypt(String data, String key) throws Exception {
    	String out = null;
		byte[] k = key.getBytes(CHARSET);
		byte[] d = data.getBytes(CHARSET);
		out = new String(decrypt(d,k), CHARSET);
    	return out;
    }
}
